import { IsNotEmpty, IsString,IsNumber } from "class-validator";
export class createTaskDto {
  @IsString()
  @IsNotEmpty()
    title: String;
  @IsString()
  @IsNotEmpty()
    description: String;
  @IsNotEmpty()
    priority: number;
}