import { EntityRepository, Repository } from "typeorm";
import { createTaskDto } from "./create-task.dto";
import { Task } from "./task.entity";
import { TaskStatus } from "./task.model";
import { UpdateTaskDTO } from "./updateStatus.dto";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task> {

  async createTask(createTaskDto: createTaskDto): Promise<Task>{
    const { title,description,priority } = createTaskDto;

    const task = new Task();

    task.title = title;
    task.description = description;
    task.status = TaskStatus.OPEN;
    task.priority = priority;

    await task.save();

    return task;
  }

  async updateTaskStatus(updateStatusDTO: UpdateTaskDTO)
  {
    const { status,id } = updateStatusDTO;
    const task = await this.findOne(id);
    task.status = status;

    await task.save();

    return task;
  }

}
