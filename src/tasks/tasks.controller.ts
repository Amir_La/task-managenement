import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Query, UsePipes, ValidationPipe, HttpCode } from '@nestjs/common';
import { IsNotEmpty, Validator } from 'class-validator';
import { createTaskDto } from './create-task.dto';
import { Task } from './task.entity';
import { TaskStatus } from './task.model';
import { TasksService } from './tasks.service';
import { UpdateTaskDTO } from './updateStatus.dto';

@Controller('tasks')
export class TasksController {
  constructor(private taskService: TasksService) {}
  
  @Get('getAllTasks')
  getAllTasks(): Promise<Task[]> {
    console.log("GET ALL TASK");
    return this.taskService.getAllTasks();
  }

  @Get('/:id')
  getTaskById(@Param('id',ParseIntPipe)id: number): Promise<Task>{
    return this.taskService.getTaskById(id);
  }

  @Post('addTask')
  @HttpCode(201)
  @UsePipes(ValidationPipe)
  addTask(@Body() createTaskDto: createTaskDto){
   if(createTaskDto)
   {
    
    return this.taskService.addTask(createTaskDto);
   }
  }

  
  @Delete(':id')
  deleteTaskById(@Param() params): Promise<Task[]> {
    console.log("DELETE TASK");
    return this.taskService.deleteTaskById(params.id);
  }

  @Patch('/updateTaskStatus')
  @UsePipes(ValidationPipe)
  updateTaskStatus(@Body() updateStatusDTO: UpdateTaskDTO): Promise<Task>{
    console.log("UPDATE TASK STATUS");
    return this.taskService.updateTaskStatusById(updateStatusDTO);
  }
 /*  @Get()
  getTasks(@Query('title') title: string,@Query('status') status: string): Task[] {
    if(title && status)
    {
      if(Object.keys(TaskStatus).find(stat => stat == status) != undefined)
      {
        return this.taskService.getAllTasksByKeywordAndStatus(title,status);
      }
      else{
        return [];
      }
    }
    else if(title)
    {
      return this.taskService.getAllTasksByTitle(title);
    }
    else if(status)
    {
      if(Object.keys(TaskStatus).find(stat => stat == status) != undefined) return this.taskService.getAllTasksByStatus(status);
    }
    else{
      return this.taskService.getAllTasks();
    }
  }
  

  @Get('getAllTasks')
  getAllTask(): Task[] {
    return this.taskService.getAllTasks();
  }
  
  @Get(':id')
  getTaskById(@Param() params): Task {
    return this.taskService.getTaskById(params.id);
  }

  @Delete(':id')
  deleteTaskById(@Param() params): Task[] {
    return this.taskService.deleteTaskById(params.id);
  }

  @Patch('/updateTaskStatus')
  @UsePipes(ValidationPipe)
  updateTaskStatus(@Body() updateStatusDTO: UpdateTaskDTO): Task[]{
    return this.taskService.updateTaskStatusById(updateStatusDTO);
  }

  @Post('addTask')
  @UsePipes(ValidationPipe)
  addTask(@Body() createTaskDto: createTaskDto ){
   if(createTaskDto)
   {
    return this.taskService.addTask(createTaskDto);
   }
  } */
}
