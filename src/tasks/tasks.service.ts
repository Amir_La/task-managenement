import { Injectable, NotFoundException } from '@nestjs/common';
import { Task } from './task.entity';
import { v1 as uuid } from 'uuid';
import { TaskStatus } from './task.model';
import { createTaskDto } from './create-task.dto';
import { UpdateTaskDTO } from './updateStatus.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(TaskRepository)
     private TaskRepository: 
     TaskRepository
  ) {}
  private tasks: Task[] = [];

  async getTaskById(id: number): Promise<Task>{
    const found = await this.TaskRepository.findOne(id);

    if(!found)
    {
      throw new NotFoundException(`Task With id = ${id} NOT FOUND`);
    }

    return found;
  }

  async addTask(task: createTaskDto): Promise<Task>{
    const newTask = await this.TaskRepository.createTask(task);
    return newTask;
  }

  async getAllTasks(): Promise<Task[]>{
    const allTasks = await this.TaskRepository.find();

    if(!allTasks)
    {
      throw new NotFoundException("NOTHING ON DB");
    }
    return allTasks;
  }

  async deleteTaskById(id: number): Promise<Task[]>{
    if(this.getTaskById(id))
    {
      await this.TaskRepository.delete(id);
    }
    // POUR FACILITER LEXEMPLE ON RETOURNE TOUTE LES DONNEES
    return this.TaskRepository.find();
  }

  async updateTaskStatusById(updateStatusDTO: UpdateTaskDTO): Promise<Task>{
    const taskEdited = await this.TaskRepository.updateTaskStatus(updateStatusDTO);

    return taskEdited;

  }


/* 
  getAllTasks(): Task[]{
    return this.tasks;
  }
  getTaskById(id: string): Task{
    const task =  this.tasks.find(task => task.id === id);
    if(!task){
      throw new NotFoundException();
    }
    else{
      return task;
    }
  }

  addTask(task: createTaskDto){
    const newTask: Task = {
      ...task,
      id: uuid(),
      status: TaskStatus.OPEN
    }

    this.tasks.push(newTask);
    return newTask;
  }
  
  deleteTaskById(id: string): Task[]{
    if(this.getTaskById(id)){
      let newTasks: Task[] = [...this.tasks].filter(task => task.id !== id);
      this.tasks = [...newTasks];
      return newTasks;
    }
  }

  updateTaskStatusById(updateTaskDTO: UpdateTaskDTO): Task[]{
    const { id,status } = updateTaskDTO;
    let newTask: Task = this.getTaskById(id);
    let newTasks: Task[] = [...this.tasks];
    let foundedId = this.tasks.findIndex(task => task.id === id);
    newTask.status = status;
    newTasks.splice(foundedId,1,newTask);
    this.tasks = [...newTasks];
    return newTasks;
  }
  getAllTasksByKeywordAndStatus(keyword: string,status: string)
  {
    let newTasks = [...this.tasks].filter(task => task.title.includes(keyword) && task.status === task.status === TaskStatus[status]);

    return newTasks;
  }

  getAllTasksByTitle(title: string): Task[]{
    let newTasks = [...this.tasks].filter(task => task.title.includes(title));
    return newTasks;
  }

  getAllTasksByStatus(status: string): Task[]{
    let newTasks = [...this.tasks].filter(task => task.status === TaskStatus[status]);
    return newTasks;
  } */

}
